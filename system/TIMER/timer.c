#include "timer.h"
#include "sys.h"
#include "beep.h"
#include "usart.h"

/*
函数名称：void TIM1_Init(u16 psc,u16 arr)
函数功能：定时器1初始化
参		数：
					arr  重装载值
					psc  预分频系数
*/
void TIM1_Init(u16 psc,u16 arr)
{
	RCC->APB2ENR|=1<<11;//TIM1时钟使能
	RCC->APB2RSTR|=1<<11;//TIM1复位使能
	RCC->APB2RSTR&=~(1<<11);//TIM1复位失能
	TIM1->DIER|=1<<0;//允许更新中断
	STM32_SetPriority(TIM1_UP_IRQn,1,1);//设置更新中断优先级
	TIM1->CNT=0;
	TIM1->ARR=arr;
	TIM1->PSC=psc-1;
	TIM1->CR1&=~(1<<0);//关闭定时器1
}

/*
函数名称: TIM1_UP_IRQHandler
函数功能: 定时器1的更新中断服务函数
*/
void TIM1_UP_IRQHandler(void)
{
  USART1_RX_FLAG=1; //表示串口1一帧数据接收完毕
  TIM1->SR=0;
  TIM1->CR1&=~(1<<0);//关闭定时器1
}

/*
函数名称：void TIM4_Init(u16 psc,u16 arr)
函数功能：定时器4初始化
参		数：
					arr  重装载值
					psc  预分频系数
*/
void TIM4_Init(u16 psc,u16 arr)
{
	RCC->APB1ENR|=1<<2;//TIM4时钟使能
	RCC->APB1RSTR|=1<<2;//TIM4复位使能
	RCC->APB1RSTR&=~(1<<2);//TIM4复位失能
#ifdef TIM4_IN_IRQ
	TIM4->DIER|=1<<0;//允许更新中断
	STM32_SetPriority(TIM4_IRQn,1,1);
#endif
	TIM4->CNT=0;
	TIM4->ARR=arr;
	TIM4->PSC=psc-1;
	TIM4->CR1|=1<<0;//使能定时器4
}


/*
void TIM4_PWM_Init(u16 psc,u16 arr,u16 cmp)
函数功能：定时器4输出PWM波
参		数：
					arr  重装载值
					psc  预分频系数
					cmp  比较值
硬件链接：
					OC1 PB6
					OC2 PB7
					OC3 PB8
					OC4 PB9
*/
void TIM4_PWM_Init(u16 psc,u16 arr,u16 cmp)
{
	//打开时钟
	RCC->APB1ENR|=1<<2;//TIM4时钟使能
	RCC->APB2ENR|=1<<3;//GPIOB时钟使能
	RCC->APB1RSTR|=1<<2;//TIM4复位使能
	RCC->APB1RSTR&=~(1<<2);//TIM4复位失能
	//GPIOB配置
	GPIOB->CRL&=0x00FFFFFF;
	GPIOB->CRL|=0xBB000000;
	GPIOB->CRH&=0xFFFFFF00;
	GPIOB->CRH|=0x000000BB;
	//定时器4配置
	TIM4->CNT=0;
	TIM4->ARR=arr;
	TIM4->PSC=psc-1;
	TIM4->CR1|=1<<7;//重装载预装载
	//OC1配置
	TIM4->CCMR1&=~(0x3<<0);//OC1通道为输出
	TIM4->CCMR1|=0x6<<4;//PWM1模式 CNT<CCR1 有效
	TIM4->CCMR1|=1<<3;//输出比较预装载使能
	TIM4->CCER|=1<<1;//设置有效电平为低电平
	TIM4->CCER|=1<<0;//开启OC1输出
	TIM4->CCR1=cmp;
	//OC2配置
	TIM4->CCMR1&=~(0x3<<8);//OC2通道为输出
	TIM4->CCMR1|=0x6<<12;//PWM1模式 CNT<CCR1 有效
	TIM4->CCMR1|=1<<11;//输出比较预装载使能
	TIM4->CCER|=1<<5;//设置有效电平为低电平
	TIM4->CCER|=1<<4;//开启OC1输出
	TIM4->CCR2=cmp;
	//OC3配置
	TIM4->CCMR2&=~(0x3<<0);//OC3通道为输出
	TIM4->CCMR2|=0x6<<4;//PWM1模式 CNT<CCR1 有效
	TIM4->CCMR2|=1<<3;//输出比较预装载使能
	TIM4->CCER|=1<<9;//设置有效电平为低电平
	TIM4->CCER|=1<<8;//开启OC1输出
	TIM4->CCR3=cmp;
	//OC4配置
	TIM4->CCMR2&=~(0x3<<8);//OC2通道为输出
	TIM4->CCMR2|=0x6<<12;//PWM1模式 CNT<CCR1 有效
	TIM4->CCMR2|=1<<11;//输出比较预装载使能
	TIM4->CCER|=1<<13;//设置有效电平为低电平
	TIM4->CCER|=1<<12;//开启OC1输出
	TIM4->CCR4=cmp;
	
	TIM4->DIER|=1<<0;//允许更新中断
	STM32_SetPriority(TIM4_IRQn,1,1);
	
	TIM4->CR1|=1<<0;//使能定时器4
	
}	


u32 key_time=0;
void TIM4_IRQHandler(void)
{
//	static u8 dir_flag=0;//0代表自加  1代表自减
	if(TIM4->SR & 1<<0)
	{
		//key_time++;
//		if(TIM4->CCR1==1000)dir_flag=1;
//		else if(TIM4->CCR1==0)dir_flag=0;
//		if(dir_flag==1)TIM4->CCR1--;
//		else if(dir_flag==0)TIM4->CCR1++;
		USART1_RX_FLAG=1;
		TIM4->CR1&=~(1<<0);//失能定时器4
	}
	TIM4->SR=0;
}



