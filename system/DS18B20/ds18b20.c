#include "ds18b20.h"
#include <stdio.h>

/*
函数名称：void DS18B20_Init(void)
函数功能：DS18B20初始化
硬件链接：DQ->PB15
*/
void DS18B20_Init(void)
{
	RCC->APB2ENR|=1<<3;//GPIOB时钟使能
	GPIOB->CRH&=0x0FFFFFFF;
	GPIOB->CRH|=0x30000000;
	GPIOB->ODR|=1<<15;//上拉
}


/*
函数名称：void DS18B20_Reset(void)
函数功能：发送复位信号
*/
void DS18B20_Reset(void)
{
	DS18B20_OUTPUT_MODE();
	DS18B20_Out=0;
	delay_us(750);
	DS18B20_Out=1;
	delay_us(15);
}

/*
函数名称：void DS18B20_Reset(void)
函数功能：检测存在脉冲
返 回 值：0代表存在  1代表不存在
*/
u8 DS18B20_Check(void)
{
	u32 i=0;
	DS18B20_INPUT_MODE();
	while(DS18B20_In && i<80)
	{
		i++;
		delay_us(1);
	}
	if(i>=60)return 1;
	i=0;
	while(!DS18B20_In && i<240)
	{
		i++;
		delay_us(1);
	}
	if(i>=240)return 1;
	return 0;
}


/*
函数名称：void DS18B20_Reset(void)
函数功能：发送命令
*/
void DS18B20_WriteByte(u8 cmd)
{
	u8 i=0;
	DS18B20_OUTPUT_MODE();
	for(i=0;i<8;i++)
	{
		DS18B20_Out = 0;
		delay_us(2);
		DS18B20_Out = cmd & 0x01;
		delay_us(60);
		DS18B20_Out=1;
		cmd>>=1;
	}
}
/*
函数名称：u8 DS18B20_ReadByte(void)
函数功能：读取数据
返 回 值：读到的数据
*/
u8 DS18B20_ReadByte(void)
{
	u8 i,data=0;
	
	for(i=0;i<8;i++)
	{
		DS18B20_OUTPUT_MODE();
		DS18B20_Out=0;
		delay_us(2);
		DS18B20_INPUT_MODE();
		delay_us(8);
		data>>=1;
		if(DS18B20_In)data|=0x80;
		delay_us(60);	
		DS18B20_Out=1;
	}
	return data;
}




/*
函数名称：u16 DS18B20_Read_Temp(void)
函数功能：获取温度
返 回 值：温度值
*/
u16 DS18B20_Read_Temp(void)
{
	u8 temp_H,temp_L;
	u16 temp;
	DS18B20_Reset();
	if(DS18B20_Check())printf("检测失败\r\n");
	DS18B20_WriteByte(0xcc);
	DS18B20_WriteByte(0x44);
	DS18B20_Reset();
	if(DS18B20_Check())printf("检测失败\r\n");
	DS18B20_WriteByte(0xcc);
	DS18B20_WriteByte(0xbe);
	temp_L=DS18B20_ReadByte();
	temp_H=DS18B20_ReadByte();
	temp=(temp_H<<8)|temp_L;
	return temp;
}

