#include "led.h"

/*
函数名称：void LED_Init(void)
函数功能：LED初始化
硬件链接：
					LED1 -> PB6
					LED2 -> PB7
					LED3 -> PB8
					LED4 -> PB9
					低电平点亮
*/
void LED_Init(void)
{
	//打开时钟
	RCC->APB2ENR|=1<<3;//GPIOB时钟使能
	//配置寄存器
	GPIOB->CRL&=0x00FFFFFF;
	GPIOB->CRL|=0x33000000;
	GPIOB->CRH&=0xFFFFFF00;
	GPIOB->CRH|=0x00000033;
	//上拉
	GPIOB->ODR|=0xF<<6;
}
