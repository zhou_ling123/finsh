#include "delay.h"

void delay_ms(u32 time)//1ms
{
	u32 i,j,k;
	for(i=0;i<time;i++)
		for(j=0;j<100;j++)
			for(k=0;k<100;k++);
}

void delay_us(u32 time)//1us
{
	u32 i,j;
	for(i=0;i<time;i++)
		for(j=0;j<10;j++);
}


void Delay_MS_Init(void)
{
	RCC->APB1ENR|=1<<0;//TIM2时钟使能
	RCC->APB1RSTR|=1<<0;//TIM2复位使能
	RCC->APB1RSTR&=~(1<<0);//TIM2复位失能
	TIM2->PSC=7200-1;
	TIM2->CR1|=1<<0;//使能定时器4
}

void Delay_Ms(u32 time)
{
	TIM2->ARR=time*10;
	TIM2->CNT=0;
	TIM2->SR=0;
	while(!(TIM2->SR & 1<<0)){}
}


