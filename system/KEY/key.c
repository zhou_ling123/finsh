#include "key.h"
#include "delay.h"
/*
函数名称：void KEY_Init(void)
函数功能：按键初始化
硬件链接：
					S2 ->PA0
					S3 ->PA1
					S4 ->PA4
					S5 ->PA5
					S2按下为高电平  S3 S4 S5按下为低电平
*/
void KEY_Init(void)
{
	//打开时钟
	RCC->APB2ENR|=1<<2;//GPIOA时钟使能
	//配置寄存器
	GPIOA->CRL&=0xFF00FF00;
	GPIOA->CRL|=0x00880088;
	//上拉
	GPIOA->ODR|=1<<1;
	GPIOA->ODR|=3<<4;
}

/*
函数名称：u8 KEY_Scanf(void)
函数功能：获取键值
返 回 值：
					1 代表KEY_S2 按下
					2 代表KEY_S3 按下
					3 代表KEY_S4 按下
					4 代表KEY_S5 按下
					0	代表没有按键按下
*/
u8 KEY_Scanf(void)
{
	static u8 key_flag=0;//按键状态标识位 0代表没有按下 1代表按下了
	if((KEY_S2 || !KEY_S3 || !KEY_S4 || !KEY_S5) && key_flag==0)
	{
		delay_ms(20);//消抖（10ms~30ms）
		key_flag=1;
		if(KEY_S2)return 1;
		else if(!KEY_S3)return 2;
		else if(!KEY_S4)return 3;
		else if(!KEY_S5)return 4;
	}
	else
	{
		if(!KEY_S2 && KEY_S3 && KEY_S4 && KEY_S5) key_flag=0;
	}
	return 0;
}









