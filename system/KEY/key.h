#ifndef _KEY_H_
#define _KEY_H_

#include "stm32f10x.h"
#include "sys.h"
//#define KEY_S2 !!(GPIOA->IDR & 1<<0)
//#define KEY_S3 !!(GPIOA->IDR & 1<<1) 
//#define KEY_S4 !!(GPIOA->IDR & 1<<4)
//#define KEY_S5 !!(GPIOA->IDR & 1<<5)

#define KEY_S2 PAin(0)
#define KEY_S3 PAin(1)
#define KEY_S4 PAin(4)
#define KEY_S5 PAin(5)

void KEY_Init(void);
u8 KEY_Scanf(void);
#endif
