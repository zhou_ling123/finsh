#include "sys.h"
//设置优先级
void STM32_SetPriority(IRQn_Type IRQn, uint32_t PreemptPriority, uint32_t SubPriority)
{
	uint32_t EncodePriority;
	NVIC_SetPriorityGrouping(NVIC_PriorityGroup_2);//设置优先级分组
	EncodePriority = NVIC_EncodePriority (NVIC_PriorityGroup_2,PreemptPriority, SubPriority);//获取优先级编码
	NVIC_SetPriority(IRQn, EncodePriority);//设置优先级
	NVIC_EnableIRQ(IRQn);//使能中断线
}

