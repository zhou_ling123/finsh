#ifndef _USART_H_
#define _USART_H_

#include "stm32f10x.h"
#define USART1_RX_len  200
extern u8 USART1_RX_BUFF[USART1_RX_len]; 
extern u8 USART1_RX_CNT; 
extern u8 USART1_RX_FLAG; //串口接收标识位  0代表没有接收完毕 1代表接收完毕

void USART1_Init(u32 baud);
void USART1_SendString(u8 *str);
void USART_X_Init(USART_TypeDef *USARTx,u32 psc,u32 baud);
void USART_X_SendString(USART_TypeDef *USARTx,u8 *str);
#endif

