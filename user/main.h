#ifndef __MAIN_H_
#define __MAIN_H_
#include "stm32f10x.h"

typedef long (*syscall_func)(void);


#define FINSH_FUNCTION_EXPORT_CMD(name, cmd, desc)      \
const char __fsym_##cmd##_name[] __attribute__((section(".rodata.name"))) = #cmd;   \
const char __fsym_##cmd##_desc[] __attribute__((section(".rodata.name"))) = #desc;  \
const struct finsh_syscall __fsym_##name __attribute__((section("FSymTab")))= \
{                           \
		__fsym_##cmd##_name,    \
		__fsym_##cmd##_desc,    \
		(syscall_func)&name     \
};

#define MSH_CMD_EXPORT(command, desc)   \
    FINSH_FUNCTION_EXPORT_CMD(command, __cmd_##command, desc)

enum input_stat
{
    WAIT_NORMAL,
    WAIT_SPEC_KEY,
    WAIT_FUNC_KEY,
};

#define FINSH_HISTORY_LINES 5
#define FINSH_CMD_SIZE 80


/* system call table */
struct finsh_syscall
{
    const char*     name;       /* the name of system call */
    const char*     desc;       /* description of system call */
    syscall_func func;      /* the function address of system call */
};

struct finsh_shell
{
    enum input_stat stat;

    u8 echo_mode:1;

    u16 current_history;
    u16 history_count;

    char cmd_history[FINSH_HISTORY_LINES][FINSH_CMD_SIZE];


    char line[FINSH_CMD_SIZE];
    u8 line_position;
    u8 line_curpos;
};

#endif
